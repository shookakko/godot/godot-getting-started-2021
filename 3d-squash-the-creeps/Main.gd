extends Node

export(PackedScene) var mob_scene

export var mob_limit := 5

var current_mobs = 0

func _ready():
	randomize()
	$UserInterface/Retry.hide()


func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and $UserInterface/Retry.visible:
		get_tree().reload_current_scene()


func spawn_mob(mob) -> void:
	# Choose a random location on Path2D.
	var mob_spawn_location = get_node("SpawnPath/SpawnLocation")
	mob_spawn_location.unit_offset = randf()

	var player_position = $Player.transform.origin

	add_child(mob)

	# We connect the mob to the score label to update the score upon squashing a mob.
	mob.connect("squashed", $UserInterface/ScoreLabel, "_on_Mob_squashed")
	mob.connect("squashed", $UserInterface/Units, "_on_Mob_squashed",[current_mobs])
	mob.connect("squashed", self, "_on_Mob_squashed")
	mob.connect("out", self, "_on_Mob_out")
	
	mob.initialize($Player, mob_spawn_location.translation, player_position)


func _on_MobTimer_timeout():
	# Create a Mob instance and add it to the scene.
	if current_mobs < mob_limit - 1:
		spawn_mob(mob_scene.instance())
		current_mobs = current_mobs + 1
		
func _on_Mob_squashed():
	current_mobs = current_mobs - 1

func _on_Mob_out() -> void:
	current_mobs = current_mobs - 1	

func _on_Player_hit():
	$MobTimer.stop()
	$UserInterface/Retry.show()
