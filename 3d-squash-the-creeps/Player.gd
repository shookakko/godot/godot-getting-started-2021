extends KinematicBody

signal hit

# How fast the player moves in meters per second.
export var speed = 14
# Vertical impulse applied to the character upon jumping in meters per second.
export var jump_impulse = 20
# Vertical impulse applied to the character upon bouncing over a mob in meters per second.
export var bounce_impulse = 16
# The downward acceleration when in the air, in meters per second per second.
export var fall_acceleration = 75

var direction = Vector3(0, 0, 0) # Used for animation

var velocity = Vector3(0, 0, 0)

onready var GRAVITY = 10


func local_action(action: String) -> bool:
	return Input.is_action_pressed(action)


func move_forward_back(in_direction: int):
	"""
	Move the camera forward or backwards
	"""
	self.direction.z += in_direction


func move_left_right(in_direction: int):
	"""
	Move the camera to the left or right
	"""
	self.direction.x += in_direction


func _process(_delta: float):
	self.velocity = Vector3(0, self.velocity.y, 0)
	self.direction = Vector3.ZERO

	self.velocity = Vector3(0, self.velocity.y, 0)
	self.direction = Vector3.ZERO
	

	if local_action("move_right"):
		self.move_left_right(+1)
	if local_action("move_left"):
		self.move_left_right(-1)
	if local_action("move_back"):
		self.move_forward_back(+1)
	if local_action("move_forward"):
		print($Pivot.get_rotation())
		self.move_forward_back(-1)
		

func _physics_process(delta):

	if self.direction != Vector3.ZERO:
		# In the lines below, we turn the character when moving and make the animation play faster.
		self.direction = self.direction.normalized()
		$Pivot.look_at(translation + self.direction, Vector3.UP)
		$AnimationPlayer.playback_speed = 4
	else:
		$AnimationPlayer.playback_speed = 1

	self.velocity.x = self.direction.x * self.speed
	self.velocity.z = self.direction.z * self.speed

	# Jumping.
	if is_on_floor() and Input.is_action_just_pressed("jump"):
		self.velocity.y += jump_impulse

	# We apply gravity every frame so the character always collides with the ground when moving.
	# This is necessary for the is_on_floor() function to work as a body can always detect
	# the floor, walls, etc. when a collision happens the same frame.
	self.velocity.y -= fall_acceleration * delta
	self.velocity = move_and_slide(velocity, Vector3.UP)

	# Here, we check if we landed on top of a mob and if so, we kill it and bounce.
	# With move_and_slide(), Godot makes the body move sometimes multiple times in a row to
	# smooth out the character's motion. So we have to loop over all collisions that may have
	# happened.
	# If there are no "slides" this frame, the loop below won't run.
	for index in range(get_slide_count()):
		var collision = get_slide_collision(index)
		if collision.collider.is_in_group("mob"):
			var mob = collision.collider
			if Vector3.UP.dot(collision.normal) > 0.1:
				mob.squash()
				self.velocity.y = bounce_impulse

	# This makes the character follow a nice arc when jumping
	$Pivot.rotation.x = PI / 6 * self.velocity.y / jump_impulse


func die():
	emit_signal("hit")
	queue_free()

func _on_MobDetector_body_entered(_body):
	die()
